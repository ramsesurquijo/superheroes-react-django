import React from 'react';
import {Route} from 'react-router-dom';
import HeroList from './containers/HeroListView';
import HeroDetail from './containers/HeroDetailView';
import ComicList from './containers/ComicListView';
import ComicDetail from './containers/ComicDetailView';



const BaseRouter = () => (
    <div>
        <Route exact path='/hero' component={HeroList} />
        <Route exact path='/hero/:id' component={HeroDetail} />
        <Route exact path='/comic' component={ComicList} />
        <Route exact path='/comic/:id' component={ComicDetail} />



    </div>
)
export default BaseRouter;