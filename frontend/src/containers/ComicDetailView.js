import React, {Component} from 'react';
import axios from 'axios';
import {Card, Button, Form} from 'antd'
import FormComic from '../components/FormComic';

class ComicDetail extends Component {

    state = {
        comic: {}
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        axios.get(`http://127.0.0.1:8000/api/comic/${id}`)
            .then(res => {
                this.setState({
                    comic: res.data
                });
        //  console.log(this.state.comic);
        })
    }

    handleDelete = (event) => {
        const id = this.props.match.params.id;
        axios.delete(`http://127.0.0.1:8000/api/hero/${id}`)
        this.props.history.push('/');
        this.forceUpdate();
    }

    render() {
    var comics = this.state.comic
        return(
            <div>
                <Card title={comics.comicName}>
                    <p>Hero Name: {comics.hero}</p>
                    <p>Description: {comics.description}</p>
                </Card>
                <FormComic
                    requestType="put"
                    id={this.props.match.params.id}
                    btnText="Edit"
                />
                <Form onSubmit={this.handleDelete}>
                    <Button type="danger" htmlType="submit">Delete</Button>
                </Form>
            </div>
        );
        
    }
}
export default ComicDetail;