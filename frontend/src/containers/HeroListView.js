import React, {Component} from 'react';
import axios from 'axios';
import Hero from '../components/Hero';
import CustomForm from '../components/Form';

class HeroList extends Component {

    state = {
        heroes: [],
        totalResults: 0
    }

    componentDidMount() {
      const urlHero = 'http://127.0.0.1:8000/api/hero/';
      axios.get(urlHero)
        .then(res => {
          this.setState({
            heroes: res.data.results,
            totalResults: res.data.count
          })
      })
    }

    getHeroesList = (page) => {
      const urlHero = `http://127.0.0.1:8000/api/hero/?page=${page}`;
      axios.get(urlHero)
        .then(res => {
          this.setState({
            heroes: res.data.results,
            totalResults: res.data.count
          })
      })
    }

    render() {
        return(
          <div>
            <Hero data={this.state.heroes} totalResults={this.state.totalResults} getHeroesList={this.getHeroesList}/>
            <hr />
            <h2>Create a Hero</h2>
            <CustomForm 
              requestType="post"
              id={null}
              btnText="Create"
            />
          </div>
        );
    }
}

export default HeroList;