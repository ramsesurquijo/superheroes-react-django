import React, {Component} from 'react';
import axios from 'axios';
import Comic from '../components/Comic';
import ComicForm from '../components/FormComic';

class ComicList extends Component {

    state = {
        comics: [],
        totalResults: 0
    }

    componentDidMount() {
      const urlComic = 'http://127.0.0.1:8000/api/comic/';
      axios.get(urlComic)
        .then(res => {
          this.setState({
            comics: res.data.results,
            totalResults: res.data.count
          })
      })
    }

    getComicsList = (page) => {
      const urlComic = `http://127.0.0.1:8000/api/comic/?page=${page}`;
      axios.get(urlComic)
        .then(res => {
          this.setState({
            comics: res.data.results,
            totalResults: res.data.count
          })
      })
    }

    render() {
        return(
          <div>
            <Comic data={this.state.comics} totalResults={this.state.totalResults} getComicsList={this.getComicsList}/>
            <hr />
            <h2>Create a Comic</h2>
            <ComicForm 
              requestType="post"
              id={null}
              btnText="Create"
            />
          </div>
        );
    }
}

export default ComicList;