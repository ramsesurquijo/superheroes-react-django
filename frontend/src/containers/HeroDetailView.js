import React, {Component} from 'react';
import axios from 'axios';
import {Card, Button, Form} from 'antd'
import CustomForm from '../components/Form';

class HeroDetail extends Component {

    state = {
        hero: {}
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        axios.get(`http://127.0.0.1:8000/api/hero/${id}`)
            .then(res => {
                this.setState({
                    hero: res.data
                })
         console.log(this.state.hero);
        })
    }

    handleDelete = (event) => {
        const id = this.props.match.params.id;
        axios.delete(`http://127.0.0.1:8000/api/hero/${id}`);
        this.props.history.push('/');
        this.forceUpdate();
    }

    render() {
    var heroes = this.state.hero
        return(
            <div>
                <Card title={heroes.name}>
                    <p>Hero Name: {heroes.heroName}</p>
                    <p>Description: {heroes.description}</p>
                </Card>
                <CustomForm 
                    requestType="put"
                    id={this.props.match.params.id}
                    btnText="Edit"
                />
                <Form onSubmit={this.handleDelete}>
                    <Button type="danger" htmlType="submit">Delete</Button>
                </Form>
            </div>
        );
        
    }
}
export default HeroDetail;