import React from 'react';
import {Link} from 'react-router-dom'


import { Layout, Menu, Breadcrumb } from 'antd';

const { Header, Content, Footer } = Layout;

const CustomLayout = (props) => {
    return(
        <Layout className="layout">
            <Header>
                <div className="logo" />
                <Menu theme="dark" mode="horizontal">
                <Menu.Item key="1">
                    <Link to="/hero">Heroes</Link>
                </Menu.Item>
                <Menu.Item key="2">
                    <Link to="/comic">Comic</Link>
                </Menu.Item>
                </Menu>
            </Header>
            <Content style={{ padding: '0 50px' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item><Link to="/hero">Home</Link></Breadcrumb.Item>
                <Breadcrumb.Item><Link to="/hero">List</Link></Breadcrumb.Item>
                </Breadcrumb>
                <div className="site-layout-content">
                    {props.children}
                </div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
        </Layout>
    );
}

export default CustomLayout;   

