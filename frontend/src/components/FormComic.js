import React, {Component} from 'react';
import axios from 'axios';
import { Form, Input, Button, Select } from 'antd';

const { Option } = Select;

const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 },
};

class ComicForm extends Component{

    state = {
        comicName: "",
        hero: "",
        description: "",
        heroes: []
    }

    componentDidMount() {
        const urlHero = `http://127.0.0.1:8000/api/hero/`;
        axios.get(urlHero)
          .then(res => {
                this.setState({
                heroes: res.data.results,
            })
        })
    }

    handleInputChanges = (event) => {
        let comicName = event.target.comicName;
        let value = event.target.value;
        this.setState({[comicName]:value})

    }

    handleFormSubmit = (requestType, id) => {
        const comicName = this.state.comicName;
        const hero = this.state.hero;
        const description = this.state.description;
        switch ( requestType ){
            case 'post':
                return axios.post('http://127.0.0.1:8000/api/comic/', {
                    comicName: comicName,
                    hero: hero,
                    description: description
                })
                .then(res => console.log(res))  
                .catch(error => console.log(error));
            case 'put':
                return axios.put(`http://127.0.0.1:8000/api/comic/${id}/`, {
                    comicName: comicName,
                    hero: hero,
                    description: description
                })
                .then(res => console.log(res))
                .catch(error => console.log(error));
            default:
                //nothing
        }
    }

    render(){   
        return (
            <div>
            <Form
                {...formItemLayout}
            > 
                <Form.Item name="name" label="Real Name">
                    <Input onChange={this.handleInputChanges} name="comicName" value={this.state.comicName} placeholder="input comic Name" />
                </Form.Item>

                <Form.Item name="heroName" label="Hero Name">
                <Select
                    onChange={this.handleInputChanges}
                    name="hero" 
                    placeholder="Select a Hero"
                >
                    {this.state.heroes.map(p => <Option value={p.hero}>{p.heroName}</Option>)}
                    {/* <Option key={this.state.heroes.id} value={this.state.hero}>
                        {this.state.heroes.id}                        
                    </Option> */}
                    {/* <Option value={this.state.hero}></Option> */}
                </Select>
                    {/* <Input onChange={this.handleInputChanges} name="hero" value={this.state.hero} placeholder="Hero name" /> */}
                </Form.Item>

                <Form.Item name="description" label="Description">
                    <Input onChange={this.handleInputChanges} value={this.state.description} name="description" />
                </Form.Item>

                <Form.Item wrapperCol={{ span: 12, offset: 6 }}>
                    <Button onClick={(event) =>{this.handleFormSubmit (this.props.requestType, this.props.id)} } type="primary" shape="round" htmlType="submit">{this.props.btnText}</Button>
                </Form.Item>
            </Form>
            </div>
        );
    }
}
export default ComicForm;
