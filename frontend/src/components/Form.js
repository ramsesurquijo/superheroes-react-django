import React, {Component} from 'react';
import axios from 'axios';
import { Form, Input, Button } from 'antd';

const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 },
};

class CustomForm extends Component{

    state = {
        name: "",
        heroName: "",
        description: ""
    }

    handleInputChanges = (event) => {
        let name = event.target.name;
        let value = event.target.value;
        this.setState({[name]:value})

    }

    handleFormSubmit = (requestType, id) => {
        const name = this.state.name;
        const heroName = this.state.heroName;
        const description = this.state.description;
        switch ( requestType ){
            case 'post':
                return axios.post('http://127.0.0.1:8000/api/hero/', {
                    name: name,
                    heroName: heroName,
                    description: description
                })
                .then(res => console.log(res))  
                .catch(error => console.log(error));
            case 'put':
                return axios.put(`http://127.0.0.1:8000/api/hero/${id}/`, {
                    name: name,
                    heroName: heroName,
                    description: description
                })
                .then(res => console.log(res))
                .catch(error => console.log(error));
            default:
                //nothing
        }
    }

    render(){   
        return (
            <div>
            <Form 
                {...formItemLayout}
            > 
                <Form.Item name="name" label="Real Name">
                    <Input onChange={this.handleInputChanges} name="name" value={this.state.name} placeholder="input name" />
                </Form.Item>

                <Form.Item name="heroName" label="Hero Name">
                    <Input onChange={this.handleInputChanges} name="heroName" value={this.state.heroName} placeholder="Hero name" />
                </Form.Item>

                <Form.Item name="description" label="Description">
                    <Input onChange={this.handleInputChanges} value={this.state.description} name="description" />
                </Form.Item>

                <Form.Item wrapperCol={{ span: 12, offset: 6 }}>
                    <Button onClick={(event) =>{this.handleFormSubmit (this.props.requestType, this.props.id)} } href='/hero' type="primary" shape="round" htmlType="submit">{this.props.btnText}</Button>
                </Form.Item>
            </Form>
            </div>
        );
    }
}
export default CustomForm;
