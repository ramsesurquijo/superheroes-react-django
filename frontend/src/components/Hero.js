import React from 'react';

import { List } from 'antd';
import { MessageOutlined, LikeOutlined, StarOutlined } from '@ant-design/icons';



const IconText = ({ icon, text }) => (
  <span>
    {React.createElement(icon, { style: { marginRight: 8 } })}
    {text}
  </span>
);

const Hero = (props) => {
    return(
        <List
            itemLayout="vertical"
            size="large"
            pagination={{
            onChange: page => {
                props.getHeroesList(page)
            },
            pageSize: 3,
            total: props.totalResults 
            }}
            dataSource={props.data}
            
            renderItem={item => (
            <List.Item
                key={item.name}
                actions={[
                <IconText icon={StarOutlined} text="156" key="list-vertical-star-o" />,
                <IconText icon={LikeOutlined} text="156" key="list-vertical-like-o" />,
                <IconText icon={MessageOutlined} text="2" key="list-vertical-message" />,
                ]}
                extra={
                <img
                    width={272}
                    alt="logo"
                    src="https://diariolahuella.com/wp-content/uploads/2020/04/marvel-comics.jpg"
                />
                }
            >
                <List.Item.Meta
                    title={<a href={`/hero/${item.id}`}>{item.name}</a>}
                    description={item.heroName}
                />
                {item.description}

            </List.Item>
            )}
        />
    );
}
export default Hero; 