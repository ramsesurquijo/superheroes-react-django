# from rest_framework import serializers
from rest_framework.serializers import (
    ModelSerializer,
)

from src.models.hero import Hero

class HeroSerializer(ModelSerializer):
    class Meta:
        model = Hero
        fields = ('id', 'name', 'heroName', 'description')

class HeroDetailSerializer(ModelSerializer):
    class Meta:
        model = Hero
        fields = '__all__' #De esta manera se agregan todos los campos fields

        