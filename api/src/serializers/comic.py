# from rest_framework import serializers (importa TODO del serializers)
from rest_framework.serializers import (
    SerializerMethodField,
    CharField,
    ModelSerializer,
    ReadOnlyField
)
from src.models.comic import Comic
from src.models.hero import Hero    
from src.serializers.hero import HeroSerializer


class ComicDetailSerializer(ModelSerializer):
    class Meta: 
        model = Comic
        fields = ('id', 'comicName', 'description', 'hero')
        depth = 1

class ComicSerializer(ModelSerializer):
    hero = Hero()
    #Dos opciones de usar foreign key serializada
    hero_name = ReadOnlyField(source='hero.heroName')
    real_name = CharField(read_only=True, source='hero.name')
    class Meta:
        model = Comic
        fields = ('id', 'comicName', 'description', 'hero', 'hero_name','real_name',)
        read_only_fields = ('id',)

