from django.db import models
from django.db.models import ( 
    ImageField, 
    CharField, 
    ForeignKey,
    )
from src.models.hero import Hero 

#My Model 
class Comic(models.Model):  
    comicName = models.CharField(max_length=100)
    hero = models.ForeignKey(Hero, on_delete=models.CASCADE)
    description = models.TextField()

    class Meta:
        ordering = ('comicName',)

    def __str__(self):
        return self.comicName