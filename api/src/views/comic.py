from rest_framework.viewsets import ModelViewSet

from src.models.comic import Comic
from src.serializers.comic import ComicSerializer


class ComicViewSet(ModelViewSet):
    serializer_class = ComicSerializer
    queryset = Comic.objects.all()
   
