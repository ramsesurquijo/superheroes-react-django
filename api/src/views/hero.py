from rest_framework.viewsets import ModelViewSet

from src.models.hero import Hero
from src.serializers.hero import HeroSerializer


class HeroViewSet(ModelViewSet):
    """
    A viewset for viewing and editing user instances.
    """
    serializer_class = HeroSerializer
    queryset = Hero.objects.all()
   




# from rest_framework.generics import (
#     ListAPIView,
#     CreateAPIView,
#     RetrieveAPIView,
#     DestroyAPIView,
#     UpdateAPIView,

# )
# class HeroList(ListAPIView):
#     queryset = Hero.objects.all()
#     serializer_class = HeroSerializer

# class HeroDetail(RetrieveAPIView):
#     queryset = Hero.objects.all()
#     serializer_class = HeroSerializer

# class HeroCreate(CreateAPIView):
#     queryset = Hero.objects.all()
#     serializer_class = HeroSerializer

# class HeroUpdate(UpdateAPIView):
#     queryset = Hero.objects.all()
#     serializer_class = HeroSerializer

# class HeroDelete(DestroyAPIView):
#     queryset = Hero.objects.all()
#     serializer_class = HeroSerializer
