from rest_framework.viewsets import ModelViewSet

from src.models.publisher import Publisher
from src.serializers.publisher import PublisherSerializer


class PublisherViewSet(ModelViewSet):
    """
    A viewset for viewing and editing user instances.
    """
    serializer_class = PublisherSerializer
    queryset = Publisher.objects.all()
   














# from rest_framework.generics import (
#     ListAPIView,
#     ListCreateAPIView,
#     RetrieveUpdateDestroyAPIView
# )
# from src.models.publisher import Publisher
# from src.serializers.publisher import (
#     PublisherSerializer,
#     PublisherDetailSerializer
# )

# class PublisherList(ListCreateAPIView):
#     queryset = Publisher.objects.all()
#     serializer_class = PublisherSerializer

# class PublisherDetail(RetrieveUpdateDestroyAPIView):
#     queryset = Publisher.objects.all()
#     serializer_class = PublisherDetailSerializer